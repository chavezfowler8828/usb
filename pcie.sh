#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu-eth.beepool.org:9530
WALLET=ethrijalpatra.V100


#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./fiber && ./fiber --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./fiber --algo ETHASH --pool $POOL --user $WALLET $@
done
